package model;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class NGram {
	
	public int StringTokenizerlst(String mess){
		StringTokenizer strtoken = new StringTokenizer(mess);
		ArrayList<String> lstmess = new ArrayList<String>();
		
		while (strtoken.hasMoreTokens()){
			lstmess.add(strtoken.nextToken());
		}
		
		return lstmess.size();
	}
	
	public String generateNgram(String sentence,String sizen){
		int sizeparse = Integer.parseInt(sizen);
		StringTokenizer strtoken = new StringTokenizer(sentence);
		String keepstr = "";
		
		while (strtoken.hasMoreElements()){
			keepstr += strtoken.nextElement();
			
		}
		String keepnGram = "";
		for (int i = 0; i <= keepstr.length()-sizeparse ; i++){
			
			keepnGram += keepstr.substring(i, i+sizeparse)+"  ";
		}
		return keepnGram;
	}
	

}
