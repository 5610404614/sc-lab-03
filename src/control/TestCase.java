package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.NGram;
import view.GUI;

public class TestCase {
	
	class ListenerMgr implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}
		
		
	}
	
	public static void main(String args[]){
		new TestCase();
		
	}
	public TestCase(){
		frame = new GUI();
		frame.pack();
		frame.setSize(500,300);
		frame.setVisible(true);

		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}
	public void setTestCase(){
		String inword = JOptionPane.showInputDialog("Enter your sentence : ");
		String sizeofngram = JOptionPane.showInputDialog("Enter your size of nGram :");
		NGram generateNgram = new NGram();
		int resultnGram = Integer.parseInt(sizeofngram);
		
		
		frame.setSentence(" Your sentence is : "+ inword +"  ");
		frame.setResult("Your size of nGram is :  " + resultnGram );
		frame.extendResult("\n"+"Your result from generatenGram is  : "+generateNgram.generateNgram(inword, sizeofngram));
	}
	ActionListener list;
	GUI frame;

}
