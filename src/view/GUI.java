package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame {
	private JLabel labelSentence;
	private JTextArea showsentence;
	private JLabel labelResults;
	private JTextArea showNGram;
	private JButton endButton;
	private String str;
	
    public GUI() {
	   createFrame();
	   
   }
   public void createFrame() {
	   setTitle("Generate NGram");
	   labelSentence = new JLabel("Your input sentence  : ");
	   showsentence = new JTextArea(" ");
	   labelResults = new JLabel("Your results is :");
	   showNGram = new JTextArea(" ");
	   
	   endButton = new JButton("end program");
	   setLayout(new BorderLayout());
	   add(labelSentence, BorderLayout.NORTH);
	   add(showsentence,BorderLayout.NORTH);
	   add(showNGram,BorderLayout.CENTER);
	   add(endButton, BorderLayout.SOUTH);
	   
   }
   public void setSentence(String str) {
	   this.str = str;
	   showsentence.setText(str);
   }
   public void setResult(String str) {
       this.str = str;
	   showNGram.setText(str);
	   

   }
   public void extendResult(String str) {
	   this.str = this.str+str;
	   showNGram.setText(this.str);
   }
   public void setListener(ActionListener list) {
	   endButton.addActionListener(list);
   }
   
} 